package gojsonapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type (
	auxiliaryFields struct {
		// Links to next, prev, first, last and self.
		Links *Links `json:"links,omitempty"`

		// Meta info to be rendered with the Response.
		Meta map[string]interface{} `json:"meta,omitempty"`
	}

	// Response is the API response payload container. Data can be a single object
	// or an iterable of entries
	Response struct {
		auxiliaryFields

		// isDataIterable determines whether the data bound to the Response is for a single object or an array/iterable of objects. The Set
		// method will mark this flag false, where as the Add method will mark true. Default false.
		isDataIterable bool `json:"-"`

		// Data is the default key to your api record entries.
		Data entries `json:"data"`

		// An array of errors rendered in the payload.
		Errors errors `json:"errors,omitempty"`

		// StatusCode is the http.StatusCode for this Response.
		StatusCode int `json:"-"`
	}

	// singleResponse is the response payload container used for a single data entry.
	singleResponse struct {
		auxiliaryFields

		// Data is the default key to your api record entries.
		Data interface{} `json:"data"`
	}

	// errorResponse is the response payload container used for a error payloads.
	errorResponse struct {
		auxiliaryFields

		// An array of errors rendered in the payload.
		Errors errors `json:"errors"`
	}

	// responseOptions is the struct of all available Response options
	responseOptions struct {
		data           interface{}
		dataIsIterable bool
	}

	// ResponseOption is the default func used to bind Response options
	ResponseOption func(*responseOptions)

	entries []interface{}
	errors  []*APIError

	// APIError is the struct to render the values of an API Error
	APIError struct {
		ID        string `json:"id,omitempty"`
		Code      string `json:"code,omitempty"`
		Detail    string `json:"detail"`
		Title     string `json:"title"`
		Separator string `json:"-"`
		Status    string `json:"status,omitempty"`
	}

	// APIErrorOption is the default func used to bind APIError options
	APIErrorOption func(*APIError)

	// Links contain the JsonAPI spec link values
	Links struct {
		First string `json:"first,omitempty"`
		Last  string `json:"last,omitempty"`
		Next  string `json:"next,omitempty"`
		Prev  string `json:"prev,omitempty"`
		Self  string `json:"self,omitempty"`
	}
)

// NewResponse will return a new Response pointer.
func NewResponse(opts ...ResponseOption) *Response {
	r := &Response{}
	ro := &responseOptions{}
	for _, opt := range opts {
		opt(ro)
	}

	if ro.data != nil {
		r.Set(ro.data)
		r.isDataIterable = ro.dataIsIterable
	}
	return r
}

// NewLinks will return a new Links pointer.
func NewLinks() *Links {
	return &Links{}
}

// Add will take any number of values and append to the api Responses data. The subtlety of add is that
// the response data will be deemed to be iterable. Use Set() for a top level single data object.
func (r *Response) Add(e ...interface{}) *Response {
	r.isDataIterable = true
	r.Data = append(r.Data, e...)
	return r
}

// First will return the first entry off the Response if exists, otherwise an empty interface which is nil.
func (r *Response) First() interface{} {
	var first interface{}
	if 0 < len(r.Data) {
		first = r.Data[0]
	}
	return first
}

// Clear will empty/clear the all related Response values.
func (r *Response) Clear() *Response {
	r.Data = entries{}
	r.ClearErrors()
	r.Links = nil
	r.ClearMeta()
	return r
}

// Set will take your entry and set as the only value on the api Responses data.
func (r *Response) Set(e interface{}) *Response {
	r.isDataIterable = false
	r.Data = entries{e}
	return r
}

// Check if the data bound to this response is iterable or not
func (r *Response) IsDataIterable() bool {
	return r.isDataIterable
}

// AddError will add an APIError instance to the Response. Pass any error interface and an optional title string
func (r *Response) AddError(err error, opts ...APIErrorOption) *Response {
	var apiErr *APIError
	ae, ok := err.(*APIError)
	if ok {
		apiErr = ae
	} else {
		apiErr = NewError(err.Error(), opts...)
	}
	r.Errors = append(r.Errors, apiErr)
	return r
}

// HasErrors will return true if any errors are assigned to the Response.
func (r *Response) ClearErrors() *Response {
	r.Errors = errors{}
	return r
}

// HasErrors will return true if any errors are assigned to the Response.
func (r *Response) HasErrors() bool {
	return 0 < len(r.Errors)
}

// AddMeta will bind a key value pair to the Meta map
func (r *Response) AddMeta(k string, v interface{}) *Response {
	if r.Meta == nil {
		r.Meta = map[string]interface{}{}
	}
	r.Meta[k] = v
	return r
}

// ClearMeta will nil the Meta map
func (r *Response) ClearMeta() *Response {
	r.Meta = nil
	return r
}

// DeleteMeta will delete a Meta map entry by key. Nohup on not exist.
func (r *Response) DeleteMeta(k string) *Response {
	if r.Meta != nil {
		delete(r.Meta, k)
	}
	return r
}

// SetStatusCode sets the status code for the Response. Defaults 200 OK.
func (r *Response) SetStatusCode(code int) *Response {
	r.StatusCode = code
	return r
}

// Write method will respect the http://jsonapi.org/format/ specification and ensure that no data key is returned
// on Responses with errors and single resources are not embedded within an array.
func (r *Response) Write(w io.Writer) (n int, err error) {
	payload, err := r.MarshalPayload()
	if err != nil {
		return fmt.Fprintf(w, "%s", err)
	}
	return w.Write(payload)
}

// WriteHTTPResponse method will respect the http://jsonapi.org/format/ specification and ensure that no data key is returned
// on Responses with errors and single resources are not embedded within an array. Differs from write in that
// http status codes are bound on write.
func (r *Response) WriteHTTPResponse(w http.ResponseWriter) (n int, err error) {
	// Set the reponse writer json header content type
	w.Header().Set("Content-Type", "application/json")

	// Assign the Response status code, default to OK.
	hasErrors := r.HasErrors()
	if r.StatusCode == 0 {
		if hasErrors {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			w.WriteHeader(http.StatusOK)
		}
	} else {
		w.WriteHeader(r.StatusCode)
	}

	payload, err := r.MarshalPayload()
	if err != nil {
		return fmt.Fprintf(w, "%s", err)
	}
	return w.Write(payload)
}

// MarshalPayload will return the marshaled byte array payload based on the response
// contents. Eg an error response if errors are bound.
func (r *Response) MarshalPayload() ([]byte, error) {
	return r.MarshalJSON()
}

// UnmarshalPayload takes a reader and will attempt to bind the embedded bytes to the
// key of "data" against the interface which must be a pointer to your underlying element.
func (r *Response) UnmarshalPayload(in io.Reader, elm interface{}) error {
	decoder := json.NewDecoder(in)
	decoded := NewResponse()
	err := decoder.Decode(decoded)
	if err != nil {
		return err
	}

	// The UnmarshalJSON invoked by decoder.Decode will set the data key
	// to a consistent entries{data} type so we easily check length and parse.
	var respData []byte
	if len(decoded.Data) > 0 {
		b, err := json.Marshal(decoded.Data[0])
		if err != nil {
			return err
		}
		respData = b
	}

	return json.Unmarshal(respData, elm)
}

// Create an errorResponse for error payloads.
func (r *Response) marshalErrorPayload() errorResponse {
	er := errorResponse{
		Errors: r.Errors,
	}
	er.Links = r.Links
	er.Meta = r.Meta
	return er
}

// Create a singleResponse. Note if an array of entries is passed, only
// the first entry will be returned.
func (r *Response) marshalSinglePayload() singleResponse {
	sr := singleResponse{
		Data: r.First(),
	}
	sr.Links = r.Links
	sr.Meta = r.Meta
	return sr
}

// Create a Response with iterable data. Note that on zero data (nil pointer),
// we initialise data with make to return an empty array. Return a deference
// to the pointer for Json marshalling
func (r *Response) marshalIterablePayload() Response {
	if len(r.Data) == 0 {
		r.Data = make([]interface{}, 0)
	}
	return *r
}

// MarshalJSON implements the underlying Go implementation
func (r *Response) MarshalJSON() ([]byte, error) {
	var err error
	buffer := bytes.NewBuffer(nil)
	encoder := json.NewEncoder(buffer)
	switch true {
	case r.HasErrors():
		err = encoder.Encode(r.marshalErrorPayload())
	case r.isDataIterable:
		err = encoder.Encode(r.marshalIterablePayload())
	default:
		err = encoder.Encode(r.marshalSinglePayload())
	}
	return bytes.TrimSpace(buffer.Bytes()), err
}

// UnmarshalJSON implements the underlying Go implementation
func (r *Response) UnmarshalJSON(b []byte) error {
	i := map[string]interface{}{}
	err := json.Unmarshal(b, &i)
	if err != nil {
		return err
	}

	// Check for the data key
	data, ok := i["data"]
	if ok {
		r.Data = entries{data}
		return nil
	}

	// Next check for errors
	_, ok = i["errors"]
	if ok {
		var apiErrors map[string]errors
		err = json.Unmarshal(b, &apiErrors)
		if err != nil {
			return err
		}
		r.Errors = apiErrors["errors"]
	}

	return nil
}

// WithResponseData will bind data to the new response
func WithResponseData(data interface{}) ResponseOption {
	return func(ro *responseOptions) {
		ro.data = data
	}
}

// WithResponseDataIsIterable will set whether a the data bound to the response is iterable.
func WithResponseDataIsIterable(dataIsIterable bool) ResponseOption {
	return func(ro *responseOptions) {
		ro.dataIsIterable = dataIsIterable
	}
}

// NewError will return a new APIError pointer.
func NewError(title string, opts ...APIErrorOption) *APIError {
	apiErr := &APIError{
		Title:     title,
		Separator: ":",
	}

	for _, opt := range opts {
		opt(apiErr)
	}
	return apiErr
}

// Error() implements the error interface and will return the error string value.
func (e APIError) Error() string {
	var sep string
	if e.Detail != "" {
		sep = e.Separator
	}
	return fmt.Sprintf("%s%s%s", e.Title, sep, e.Detail)
}

// WithAPIErrorID adds an optional ID to an error
func WithAPIErrorID(id string) APIErrorOption {
	return func(e *APIError) {
		e.ID = id
	}
}

// WithAPIErrorCode adds an optional code to an error
func WithAPIErrorCode(code string) APIErrorOption {
	return func(e *APIError) {
		e.Code = code
	}
}

// WithAPIErrorDetail adds an optional detail to an error
func WithAPIErrorDetail(detail string) APIErrorOption {
	return func(e *APIError) {
		e.Detail = detail
	}
}

// WithAPIErrorDetail adds an optional separator to an error
func WithAPIErrorSeparator(separator string) APIErrorOption {
	return func(e *APIError) {
		e.Separator = separator
	}
}

// WithAPIErrorDetail adds an optional string to an error
func WithAPIErrorStatus(status string) APIErrorOption {
	return func(e *APIError) {
		e.Status = status
	}
}
