package gojsonapi_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"testing"

	"reflect"

	"gitlab.com/kylehqcom/gojsonapi"
)

func TestNewResponse(t *testing.T) {
	r := gojsonapi.NewResponse()
	if reflect.TypeOf(r).Kind().String() != "ptr" {
		t.Error("Expected a pointer on new response instance.")
	}

	if reflect.TypeOf(r).Elem().String() != "gojsonapi.Response" {
		t.Error("Unknown type returned on new response instance.")
	}

	if r.HasErrors() {
		t.Error("No errors should be on a new response.")
	}

	// Also check options
	r = gojsonapi.NewResponse(gojsonapi.WithResponseData("kylehqcom"))
	if r.IsDataIterable() {
		t.Error("New response WithResponseData isDataIterable should be false.")
	}

	if r.First().(string) != "kylehqcom" {
		t.Error("New response WithResponseData should have string first entry.")
	}

	r = gojsonapi.NewResponse(gojsonapi.WithResponseData("kylehqcom"), gojsonapi.WithResponseDataIsIterable(true))
	if !r.IsDataIterable() {
		t.Error("New response WithResponseData isDataIterable should be true.")
	}

	if r.First().(string) != "kylehqcom" {
		t.Error("New response WithResponseData should have string first entry.")
	}
}

func TestNewLinks(t *testing.T) {
	l := gojsonapi.NewLinks()
	if reflect.TypeOf(l).Kind().String() != "ptr" {
		t.Error("Expected a pointer on new links instance.")
	}

	if reflect.TypeOf(l).Elem().String() != "gojsonapi.Links" {
		t.Error("Unknown type returned on new links instance.")
	}
}

func TestApiResponseLinks(t *testing.T) {
	r := gojsonapi.NewResponse()
	if nil != r.Links {
		t.Error("Should be no link instances on new api response.")
	}

	l := gojsonapi.NewLinks()
	l.First = "f"
	l.Last = "l"
	l.Next = "n"
	l.Prev = "p"
	l.Self = "s"
	r.Links = l

	if nil == r.Links {
		t.Error("Should be a links instance on api response.")
	}

	expected := `{"links":{"first":"f","last":"l","next":"n","prev":"p","self":"s"},"data":null}`
	p, err := r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}

	if expected != string(p) {
		t.Errorf("Unexpected links payload. Have: %s Expected: %s", string(p), expected)
	}
}

func TestApiResponseMeta(t *testing.T) {
	r := gojsonapi.NewResponse()
	if nil != r.Meta {
		t.Error("Should be no meta instances on new api response.")
	}

	r.DeleteMeta("count").AddMeta("count", 1).AddMeta("max_id", "max").AddMeta("min_id", "min").DeleteMeta("count")
	if nil == r.Meta {
		t.Error("Should be a meta instance on api response.")
	}

	expected := `{"meta":{"max_id":"max","min_id":"min"},"data":null}`
	p, err := r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}

	have := string(p)
	if expected != string(p) {
		t.Errorf("Unexpected meta payload. Have: %s Expected: %s", have, expected)
	}
}

func TestApiResponse_IsDataIterable(t *testing.T) {
	r := gojsonapi.NewResponse()
	if r.IsDataIterable() {
		t.Error("A new api response should be single false by default.")
	}

	expected := `{"data":null}`
	p, err := r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}

	if expected != string(p) {
		t.Error("Unexpected single empty payload")
	}
}

func TestApiResponse_SetStatusCode(t *testing.T) {
	r := gojsonapi.NewResponse()
	if r.StatusCode != 0 {
		t.Error("A new api response should have zero status code")
	}

	r.SetStatusCode(1234)
	if r.StatusCode != 1234 {
		t.Error("Response status code should be set.")
	}
}

func TestApiResponse_Add(t *testing.T) {
	r := gojsonapi.NewResponse()
	var e interface{}
	r.Add(e)
	if r.IsDataIterable() != true {
		t.Error("Add should set IsDataIterable flag to true")
	}

	r.Add(e, e, e)
	if r.IsDataIterable() != true {
		t.Error("Add should set IsDataIterable flag to true")
	}

	expects := 4
	have := len(r.Data)
	if have != expects {
		t.Errorf("Unexpected data len. Have %d Expects: %d", have, expects)
	}
}

func TestApiResponse_Set(t *testing.T) {
	r := gojsonapi.NewResponse()
	var e interface{}
	r.Set(e)
	if r.IsDataIterable() != false {
		t.Error("Set should set IsDataIterable flag to false")
	}
}

func TestApiResponse_Clear(t *testing.T) {
	r := gojsonapi.NewResponse()
	var e interface{}
	for i := 0; i < 6; i++ {
		r.Add(e)
	}
	if len(r.Data) == 5 {
		t.Error("Should have a 5 count.")
	}

	r.Clear()
	if len(r.Data) != 0 {
		t.Error("Should have a 0 count.")
	}
}

func TestApiResponse_First(t *testing.T) {
	r := gojsonapi.NewResponse()
	if nil != r.First() {
		t.Error("Expected an empty inteface/nil value.")
	}

	f := "first"
	r.Add(f)
	if f != r.First() {
		t.Error("Expected first.")
	}
}

func TestApiResponse_Count(t *testing.T) {
	r := gojsonapi.NewResponse()
	for i := 1; i < 20; i++ {
		r.Add("foo")
		if i != len(r.Data) {
			t.Error("Should have a count of: ", i)
		}
	}
}

func TestNewError(t *testing.T) {
	e := gojsonapi.NewError("err")
	if reflect.TypeOf(e).Kind().String() != "ptr" {
		t.Error("Expected a pointer on new error instance.")
	}

	if reflect.TypeOf(e).Elem().String() != "gojsonapi.APIError" {
		t.Error("Unknown type returned on new error instance.")
	}
}

func TestApiResponse_AddError(t *testing.T) {
	r := gojsonapi.NewResponse()
	if len(r.Errors) != 0 {
		t.Error("Should be no error.")
	}

	r.AddError(gojsonapi.NewError("one"))
	if len(r.Errors) != 1 {
		t.Error("Should have one APIError.")
	}

	r.AddError(fmt.Errorf("Regular error interface"))
	if len(r.Errors) != 2 {
		t.Error("Should have 2 errors")
	}
}

func TestApiResponse_ErrorToString(t *testing.T) {
	err := gojsonapi.NewError("title")
	if fmt.Sprintf("%s", err) != "title" {
		t.Error("String error should be 'title'")
	}
	err.Detail = "details"
	expects := "title:details"
	has := fmt.Sprintf("%s", err)
	if expects != has {
		t.Errorf("String error expects: %s has: %s", expects, has)
	}

	err = gojsonapi.NewError("woot", gojsonapi.WithAPIErrorSeparator("+++"), gojsonapi.WithAPIErrorDetail("yay"))
	expects = "woot+++yay"
	has = fmt.Sprintf("%s", err)
	if expects != has {
		t.Errorf("String error expects: %s has: %s", expects, has)
	}
}

func TestApiResponse_HasErrors(t *testing.T) {
	r := gojsonapi.NewResponse()
	if r.HasErrors() {
		t.Error("Should be no error.")
	}

	r.AddError(gojsonapi.NewError("one"))
	if !r.HasErrors() {
		t.Error("Should be one error.")
	}
}

func TestGenerateErrorPayload(t *testing.T) {
	r := gojsonapi.NewResponse()
	e := gojsonapi.NewError(
		"Oh dear..",
		gojsonapi.WithAPIErrorCode("abc"),
		gojsonapi.WithAPIErrorDetail("I pity the fool."),
		gojsonapi.WithAPIErrorID("error_id"),
		gojsonapi.WithAPIErrorStatus("418"),
	)
	r.AddError(e)
	r.AddMeta("count", 10)
	l := gojsonapi.NewLinks()
	l.First = "10"
	r.Links = l
	p, err := r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}
	singleExpected := `{"id":"error_id","code":"abc","detail":"I pity the fool.","title":"Oh dear..","status":"418"}`
	singleExpectedFull := fmt.Sprintf(`{"links":{"first":"10"},"meta":{"count":10},"errors":[%s]}`, singleExpected)
	if singleExpectedFull != string(p) {
		t.Errorf("Unexpected payload on single error. Got: %s Expected: %s", string(p), singleExpectedFull)
	}

	r.AddError(e)
	p, err = r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}
	doubleExpected := fmt.Sprintf(`{"links":{"first":"10"},"meta":{"count":10},"errors":[%s,%s]}`, singleExpected, singleExpected)
	if doubleExpected != string(p) {
		t.Errorf("Unexpected payload on double error. Got: %s Expected: %s", string(p), doubleExpected)
	}
}

func TestGenerateSinglePayload(t *testing.T) {
	r := gojsonapi.NewResponse()
	r.AddMeta("count", 10)
	l := gojsonapi.NewLinks()
	l.First = "10"
	r.Links = l
	p, err := r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}
	expected := `{"links":{"first":"10"},"meta":{"count":10},"data":null}`
	if expected != string(p) {
		t.Errorf("Unexpected payload for no entry. Got: %s Expected: %s", string(p), expected)
	}

	r.Set("first")
	r.Set("second")

	// second is the last set which will overwrite "first"
	p, err = r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}
	expected = `{"links":{"first":"10"},"meta":{"count":10},"data":"second"}`
	if expected != string(p) {
		t.Errorf("Unexpected payload forsingleno entry. Got: %s Expected: %s", string(p), expected)
	}
}

func TestGenerateIterablePayload(t *testing.T) {
	r := gojsonapi.NewResponse()
	r.AddMeta("count", 0)
	l := gojsonapi.NewLinks()
	l.First = "10"
	r.Links = l
	p, err := r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}
	expected := `{"links":{"first":"10"},"meta":{"count":0},"data":null}`
	if expected != string(p) {
		t.Errorf("Unexpected payload for no entry. Got: %s Expected: %s", string(p), expected)
	}

	r.Add("first")
	p, err = r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}

	expected = `{"links":{"first":"10"},"meta":{"count":0},"data":["first"]}`
	if expected != string(p) {
		t.Errorf("Unexpected payload for multiple set entries. Got: %s Expected: %s", string(p), expected)
	}

	r.Add("second")
	p, err = r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}

	expected = `{"links":{"first":"10"},"meta":{"count":0},"data":["first","second"]}`
	if expected != string(p) {
		t.Errorf("Unexpected payload for Iterable Payload. Got: %s Expected: %s", string(p), expected)
	}
}

func TestMarshalJSON(t *testing.T) {
	r := gojsonapi.NewResponse()
	r.AddMeta("count", 0)
	l := gojsonapi.NewLinks()
	l.First = "10"
	r.Links = l
	p, err := r.MarshalPayload()
	if err != nil {
		t.Error(err)
	}

	// Check on calling Marshal directly
	pp, err := json.Marshal(r)
	if err != nil {
		t.Error(err)
	}

	if !bytes.Equal(p, pp) {
		t.Error("A response MarshalPayload and json.Marshal should be equal")
	}

	// Also ensure encoders give same result also
	buffer := bytes.NewBuffer(nil)
	encoder := json.NewEncoder(buffer)
	err = encoder.Encode(r)
	if err != nil {
		t.Error(err)
	}
	if !bytes.Equal(bytes.TrimSpace(buffer.Bytes()), pp) {
		t.Error("A response MarshalPayload and json.Marshal should be equal")
	}
}

type (
	AThing struct {
		Name string `json:"name"`
		Num  int64  `json:"num"`
	}

	Things []*AThing
)

func TestUnmarshalPayload(t *testing.T) {
	assert := &AThing{Name: "unmarshal", Num: 42}
	singleResp := gojsonapi.NewResponse()
	singleResp.Set(assert)
	payload, err := singleResp.MarshalPayload()
	if err != nil {
		t.Error(err)
	}

	// Create a buffer as an io.Reader
	b := bytes.NewBuffer(nil)
	b.Write(payload)

	thing := &AThing{}
	err = singleResp.UnmarshalPayload(b, thing)
	if err != nil {
		t.Error(err)
	}

	// Assert the thing
	if !reflect.DeepEqual(assert, thing) {
		t.Error("The AThing does not match after UnmarshalPayload")
	}

	if singleResp.HasErrors() {
		t.Errorf("Unmarshal decode has errors. Should be empty. Have: %s", singleResp.Errors[0])
	}

	// Also ensure that Unmarshal works with iterable data
	iterDataResp := gojsonapi.NewResponse()
	from := Things{
		assert,
		{Name: "unmarshal2", Num: 84},
	}
	for _, a := range from {
		iterDataResp.Add(a)
	}

	payload, err = iterDataResp.MarshalPayload()
	if err != nil {
		t.Error(err)
	}

	// Write to the buffer so we can assert UnmarshalPayload
	b.Reset()
	b.Write(payload)
	var to Things
	err = iterDataResp.UnmarshalPayload(b, &to)
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(from, to) {
		t.Error("The To & From Things do not match after UnmarshalPayload")
	}
}

func TestUnmarshalJSON(t *testing.T) {
	a := AThing{
		Name: "ace",
		Num:  3,
	}

	resp := gojsonapi.NewResponse()
	resp.Set(a)
	payload, err := resp.MarshalPayload()
	if err != nil {
		t.Errorf("Payload generation error: %s", err)
	}

	// Also check that multiple calls returns same payload
	payload2, err := resp.MarshalPayload()
	if err != nil {
		t.Errorf("Payload generation error: %s", err)
	}

	if !bytes.Equal(payload2, payload) {
		t.Errorf("Duplicate calls to MarshalPayload should return same payload. Got: %s Expected: %s", payload2, payload)
	}

	decoder := json.NewDecoder(bytes.NewBuffer(payload))
	decoded := gojsonapi.NewResponse()
	err = decoder.Decode(decoded)
	if err != nil {
		t.Errorf("Decoding error: %s", err)
	}

	// Ensure no errors bound either
	if decoded.HasErrors() {
		t.Errorf("Unmarshal decode has errors. Should be empty. Have: %s", decoded.Errors[0])
	}

	// All good on a single payload, lets test iterable data too
	resp.Clear()
	from := Things{
		&a,
		{Name: "base", Num: 6},
	}
	for _, f := range from {
		resp.Add(f)
	}

	payload, err = resp.MarshalPayload()
	if err != nil {
		t.Errorf("Payload generation error: %s", err)
	}

	decoder = json.NewDecoder(bytes.NewBuffer(payload))
	decoded = gojsonapi.NewResponse()
	err = decoder.Decode(decoded)
	if err != nil {
		t.Errorf("Decoding error: %s", err)
	}

	payload2, err = decoded.MarshalPayload()
	if err != nil {
		t.Errorf("Payload generation error: %s", err)
	}

	// Ensure no errors bound either
	if decoded.HasErrors() {
		t.Errorf("Unmarshal decode has errors. Should be empty. Have: %s", decoded.Errors[0])
	}

	if !bytes.Equal(payload2, payload) {
		t.Errorf("Duplicate calls to MarshalPayload should return same payload. Got: %s Expected: %s", payload2, payload)
	}

}

func TestUnmarshalJSONErrorPayload(t *testing.T) {
	// Check a response with errors
	resp := gojsonapi.NewResponse()
	apiError := &gojsonapi.APIError{
		Detail: "Such detail",
		Title:  "Kaboom",
	}

	resp.AddError(apiError)
	payload, err := resp.MarshalPayload()
	if err != nil {
		t.Errorf("Payload generation error: %s", err)
	}

	decoder := json.NewDecoder(bytes.NewBuffer(payload))
	decoded := gojsonapi.NewResponse()
	err = decoder.Decode(decoded)
	if err != nil {
		t.Errorf("Decoding error: %s", err)
	}

	// Ensure we have errors
	if !decoded.HasErrors() {
		t.Error("Expected a response errors after decoding")
	}

	if len(decoded.Errors) > 1 {
		t.Errorf("Expected an error count of 1. Got: %d", len(decoded.Errors))
	}

	decodedApiError := decoded.Errors[0]
	if apiError.Detail != decodedApiError.Detail {
		t.Errorf("Unmarshal decode error. Got: %s Expected: %s", apiError.Detail, decodedApiError.Detail)
	}

	if apiError.Title != decodedApiError.Title {
		t.Errorf("Unmarshal decode error. Got: %s Expected: %s", apiError.Title, decodedApiError.Title)
	}
}

func TestWrite(t *testing.T) {
	expects := &AThing{Name: "unmarshal", Num: 42}
	resp := gojsonapi.NewResponse()
	resp.Set(expects)

	b := bytes.NewBuffer(nil)
	n, err := resp.Write(b)
	if err != nil {
		t.Error(err)
	}
	if n == 0 {
		t.Error("Expected number of bytes written to be non zero")
	}

	assert := &AThing{}
	err = resp.UnmarshalPayload(b, assert)
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(expects, assert) {
		t.Error("The things do not match after Write")
	}
}
